# README: proj1-pageserver

**Author**: Lindsey Uribe
**Contact**: luribe@uoregon.edu

## A "trivial server" for trivial things.

### What is this repository for?

This server utilizes Python 3.4+ socket binding (https://docs.python.org/3.4/howto/sockets.html) 
to serve up very simple HTML and CSS pages from a single directory. Appropriate responses 
are served up for malicious URL attempts as well as non-existent pages. 