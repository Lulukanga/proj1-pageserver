"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  Serves up requested files, while performing minimal checks on
  malicious requests.
"""

import config    # Configure from .ini files and command line
import logging   # Better than print statements
import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program
import os
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration


def find_evil(route: str) -> bool:
    """Catch malicious routes that have //, ~ or .. in them.
    Args:
        route: A string
    Returns:
        False: if malicious URL was intercepted
        True: if we're all good in the hood
        None: if no route was received (Not implemented error 401)
    """
    result = True
    dub_slashes = route.split("//")
    if len(dub_slashes) > 1:
        # end1 = dub_slashes[-1]
        result = False
    else:
        sing_slashes = route.split("/")
        if sing_slashes[-1] == "":
            result = None
        else:
            end = sing_slashes[-1]
            if (end[0] == "~") or (end[0:2] == ".."):
                result = False
            elif (len(sing_slashes) >= 3) and (sing_slashes[-2] == "") and (sing_slashes[-3] == ""):
                result = False
    return result


def handle_request(route, sock):
    """Check requests, find files, transmit status, transmit files.
    Args:
        route: A string indicating what file to look for from DOCROOT.
        sock: Server socket.
    Returns:
        nothing
    """
    if find_evil(route) is None:
        # transmit unhandled exception
        log.info(f"We received something unexpected: {route}")
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        not_impl_msg = """401 Not Implemented\n"""
        transmit(not_impl_msg, sock)
        return
    elif find_evil(route) is False:
        # stop attempts to access goodies
        log.info(f"We received something malicious: {route}")
        transmit(STATUS_FORBIDDEN, sock)  # sending http status 403
        forbidden_msg = "403 Forbidden\n"
        transmit(forbidden_msg, sock)
        return
    route = route[1:]  # cut the first char off
    log.info(f"This is the cleaned route: {route}")
    options = get_options()
    file = os.path.join(options.DOCROOT, route)
    file_exists = os.path.exists(file)
    if file_exists:
        f = open(file, "r")
        file_lines = f.readlines()  # b/c an array
        file_contents = " ".join(file_lines)
        file_contents = f'{file_contents}'
        transmit(STATUS_OK, sock)  # sending http status 200 to browser
        transmit(file_contents, sock)  # sending contents of file to browser
        log.info(f"We sent this: {file}, per /{route}'s request")
    else:
        transmit(STATUS_NOT_FOUND, sock)  # sending http status 404
        not_found_msg = """404 Not Found\n"""
        transmit(not_found_msg, sock)
        log.info(f"No files were found to serve up, per /{route}'s request")
    return


def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    serversocket.bind(('', portnum))
    serversocket.listen(1)    # A real server would have multiple listeners
    return serversocket


def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:
        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))


# HTTP response codes, as the strings we will actually send.
# See:  https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
# or    http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
##
STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = """HTTP/1.0 404 Not Found\n\n"""
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"


def respond(sock):
    """
    This server responds only to GET requests (not PUT, POST, or UPDATE).
    Any valid GET request is answered with an ascii graphic of a cat.
    """
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))  # This is URL../what
    parts = request.split()
    route = parts[1]  # This is what we're looking for

    log.info(f"This is the route we received: {route}")
    if len(parts) > 1 and parts[0] == "GET":  # sending CAT
        handle_request(route, sock)
    else:
        log.info("Unhandled request: {}".format(request))
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        transmit("\nI don't handle this request: {}\n".format(request), sock)

    sock.shutdown(socket.SHUT_RDWR)
    sock.close()
    return


def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)

###
#
# Run from command line
#
###


def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING
    if options.PORT <= 1000:
        log.warning((f"Port {options.port} selected. " +
                    " Ports 0..1000 are reserved \n" + "by the operating system"))
    return options


def main():
    options = get_options()
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))
    log.info("Socket is {}".format(sock))
    serve(sock, respond)


if __name__ == "__main__":
    main()
